import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.Charset;
import java.nio.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;

/**
 * @author K.Sakamoto
 */
public class Java7FileIO {
	public Java7FileIO(String input, String output) {
		Charset charset = StandardCharsets.UTF_8;
		try (
			FileSystem fs = FileSystems.getDefault();
			BufferedReader br = Files.newBufferedReader(fs.getPath(input), charset);
			PrintWriter pw = new PrintWriter(Files.newBufferedWriter(fs.getPath(output), charset));
		) {
			String line;
			while ((line = br.readLine()) != null) {
				pw.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		new Java7FileIO("入力ファイルパス", "出力ファイルパス");
	}
}